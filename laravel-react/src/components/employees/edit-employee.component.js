import Dashboard from "../dashboard/dashboard";
import { useHistory, withRouter } from "react-router-dom";
import { useState, useEffect } from "react";
import Swal from "sweetalert2";

function UpdateEmployee(props) {
  const [data, setData] = useState([]);
  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [email, setEmail] = useState("");
  const [phonenum, setPhonenum] = useState("");
  const [file, setFile] = useState("");

  let history = useHistory();

  useEffect(async () => {
    let result = await fetch(
      "http://localhost:8000/api/employee/" + props.match.params.id
    );
    result = await result.json();
    setData(result);
    setFname(result.fname);
    setLname(result.lname);
    setEmail(result.email);
    setPhonenum(result.phonenum);
    setFile(result.file);
  }, []);

  async function editEmployee(id) {
    const formData = new FormData();

    formData.append("fname", fname);
    formData.append("lname", lname);
    formData.append("email", email);
    formData.append("phonenum", phonenum);
    formData.append("file", file);
    let result = await fetch(
      "http://localhost:8000/api/update_employee/" + id + "?_method=PUT",
      {
        method: "POST",
        body: formData,
      }
    );
    Swal.fire("Good job!", "Employee Updated Successfully", "success");
    history.push("/employees-list");
  }
  return (
    <div>
      <Dashboard />
      <div className="form-wrapper" style={{ margin: "5% 20% 0 20%" }}>
        <h1>Update Employee</h1>
        <input
          type="text"
          onChange={(e) => setFname(e.target.value)}
          defaultValue={data.fname}
          className="form-control"
        />
        <br />
        <input
          type="text"
          onChange={(e) => setLname(e.target.value)}
          defaultValue={data.lname}
          className="form-control"
        />{" "}
        <br />
        <input
          type="text"
          onChange={(e) => setEmail(e.target.value)}
          defaultValue={data.email}
          className="form-control"
        />{" "}
        <br></br>
        <input
          type="text"
          onChange={(e) => setPhonenum(e.target.value)}
          defaultValue={data.phonenum}
          className="form-control"
        />{" "}
        <br />
        <input
          placeholder="Employee photo"
          type="file"
          onChange={(e) => setFile(e.target.files[0])}
          defaultValue={data.file_path}
          className="form-control"
        />{" "}
        <br />
        <img
          style={{ width: 300 }}
          src={"http://localhost:8000/" + data.file_path}
        />
        <br /> <br />
        <button
          onClick={() => editEmployee(data.id)}
          className="btn btn-primary"
          style={{ marginBottom: "5%" }}
        >
          Update Team
        </button>
      </div>
    </div>
  );
}
export default withRouter(UpdateEmployee);
