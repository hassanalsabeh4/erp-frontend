import Dashboard from "../dashboard/dashboard";

import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
function CreateTeam() {
  let history = useHistory();
  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [email, setEmail] = useState("");
  const [phonenum, setPhonenum] = useState("");
  const [file, setFile] = useState("");

  async function addEmployee() {
    const formData = new FormData();
    formData.append("fname", fname);
    formData.append("lname", lname);
    formData.append("email", email);
    formData.append("phonenum", phonenum);
    formData.append("file", file);
    let result = await fetch("http://localhost:8000/api/addemployee", {
      method: "POST",
      body: formData,
    });    Swal.fire("Good job!", "Employee Added Successfully", "success");
    history.push("/employees-list");
  }
  return (
    <div>
      <Dashboard />
      <div className="form-wrapper" style={{ margin: "5% 20% 0 20%" }}>
        <br />
        <h1>Create Employee</h1>
        <br />

        <input
          type="text"
          onChange={(e) => setFname(e.target.value)}
          className="form-control"
          placeholder="First Name"
          required
        />
        <br />
        <input
          type="text"
          onChange={(e) => setLname(e.target.value)}
          className="form-control"
          placeholder="Last Name"
          required
        />
        <br />
        <input
          type="email"
          onChange={(e) => setEmail(e.target.value)}
          className="form-control"
          placeholder="Email"
          required
        />
        <br />
        <input
          type="number"
          onChange={(e) => setPhonenum(e.target.value)}
          className="form-control"
          placeholder="Phone Number"
          required
        />
        <br />
        <input
          type="file"
          onChange={(e) => setFile(e.target.files[0])}
          className="form-control"
          placeholder="Profile Photo"
          required
        />
        <br />

        <br />
        <button
          className="btn btn-primary"
          onClick={addEmployee}
          style={{ marginTop: "-2%", width: "150px", height: "50px" }}
        >
          Add Employee
        </button>
      </div>
    </div>
  );
}
export default CreateTeam;
