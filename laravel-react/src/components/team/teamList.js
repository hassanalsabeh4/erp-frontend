import React from "react";
import ReactDOM from "react";
import { useState, useEffect } from "react";
import Dashboard from "../dashboard/dashboard";
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import Button from "react-bootstrap/Button";
// import { useHistory } from "react-router-dom";
// import Swal from "sweetalert2";
function TeamsList() {
    // let history = useHistory();
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const [q, setQ] = useState("");
  const [searchParam] = useState(["name","description" ,"employees"]);
  const [filterParam, setFilterParam] = useState(["All"]);


  async function deleteOperation(id) {
    let result = await fetch(
      "http://localhost:8000/api/delete-team/" + id,
      {
        method: "DELETE",
      }
    );
    result = await result.json();
    // Swal.fire("Good job!", "Team Added Successfully", "success");
    // history.push("/teams-list");
    window.location.reload();
  }


  useEffect(() => {
    fetch("http://localhost:8000/api/list-team")
      .then((res) => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setItems(result);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      );
  }, []);

  function search(items) {
    return items.filter((item) => {
      if (item.name == filterParam) {
        return searchParam.some((newItem) => {
          return (
            item[newItem].toString().toLowerCase().indexOf(q.toLowerCase()) > -1
          );
        });
      } else if (filterParam == "All") {
        return searchParam.some((newItem) => {
          return (
            item[newItem].toString().toLowerCase().indexOf(q.toLowerCase()) > -1
          );
        });
      }
    });
  }

  if (error) {
    return <>{error.message}</>;
  } else if (!isLoaded) {
    return <>loading...</>;
  } else {
    return (
      <div>
        <Dashboard />
        <div className="table-wrapper" style={{ margin: "5% 10% 0 20%" }}>
          <h1>Teams</h1>
          <input
          style={{width:"200px" ,marginBottom:"4%"}}
            type="text"
            name="search-form"
            id="search-form"
            className="form-control"
            placeholder="Search for..."
            value={q}
            onChange={(e) => setQ(e.target.value)}
          />
       

       <Table striped bordered hover>
          <thead>
            <tr>
              <th>Id</th>
              <th>Team Name</th>
              <th>Team Description</th>
              <th>Employee</th>
              <th style={{ width: "120px" }}>Logo</th>
              <th style={{ width: "80px" }}>Delete</th>
              <th style={{ width: "80px" }}>Edit</th>
            </tr>
          </thead>
          <tbody>
            {search(items).map((item) => (
              <tr>
              <td>{item.id}</td>
              <td>{item.name}</td>
              <td>{item.description}</td>
              <td>{item.employees}</td>
              <td>
                <img
                  style={{ width: "100px", height: "50px" }}
                  src={"http://localhost:8000/" + item.file_path}
                ></img>
              </td>
              <td>
                <Link
                  className="edit-link"
                  onClick={() => deleteOperation(item.id)}
                >
                  <Button size="sm" variant="danger">
                    Delete
                  </Button>
                </Link>
              </td>
              <td>
                {" "}
                <Link to={"/teams-update/" + item.id}>
                  <Button
                    size="sm"
                    variant="info"
                    style={{ marginRight: "10px" }}
                  >
                    Edit
                  </Button>
                </Link>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      <Link className="edit-link" to={"/teams-add"}>
          <Button
            variant="primary"
            size="lg"
            block="block"
            type="submit"
            style={{ margin: "2% 0 10% 0" }}
          >
            Add Team
          </Button>
        </Link>
        </div>
      </div>
    );
  }
}
export default TeamsList;
