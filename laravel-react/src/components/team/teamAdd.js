import Dashboard from "../dashboard/dashboard";
// import { useState } from "react";
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
function CreateTeam() {
  const [name, setName] = useState("");
  const [file, setFile] = useState("");
  const [employees, setEmployees] = useState("");
  const [description, setDescription] = useState("");
  let history = useHistory();

  //   async function getEmployees(){
  //     let result= await fetch("http://localhost:8000/api/employees");
  //     result= await result.json();
  //     setData(result)
  // }
  //   const[data,setData]=useState([]);
  //     useEffect( ()=>{
  //         getEmployees();
  //         },[])

  async function addTeam() {
    console.warn(name, file, employees, description);
    const formData = new FormData();

    formData.append("file", file);
    formData.append("name", name);
    formData.append("employees", employees);
    formData.append("description", description);
    let result = await fetch("http://localhost:8000/api/addteam", {
      method: "POST",
      body: formData,
    });
    Swal.fire("Good job!", "Team Added Successfully", "success");
    history.push("/teams-list");
  }
  return (
    <div>
      <Dashboard />
      <div className="form-wrapper" style={{ margin: "5% 20% 0 20%" }}>
        <br />
        <h1>Create a Team</h1>
        <br />
        <input
          type="text"
          onChange={(e) => setName(e.target.value)}
          className="form-control"
          placeholder="Name"
        />
        <br />
        <input
          type="file"
          onChange={(e) => setFile(e.target.files[0])}
          className="form-control"
          placeholder="Team's logo"
        />
        <br />
        <input
          type="text"
          onChange={(e) => setEmployees(e.target.value)}
          className="form-control"
          placeholder="Employees"
        />
        <br />
        <input
          type="text"
          onChange={(e) => setDescription(e.target.value)}
          className="form-control"
          placeholder="Descirption"
        />
        <br />
      </div>
      <br />
      <br />
      <div className="col-sm-6 offset-sm-3">
        <button
          className="btn btn-primary"
          onClick={addTeam}
          style={{
            marginTop: "-10%",
            marginLeft: "-10%",
            width: "150px",
            height: "50px",
          }}
        >
          Add team
        </button>
      </div>
    </div>
  );
}
export default CreateTeam;
