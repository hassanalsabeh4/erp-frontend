import Dashboard from "../dashboard/dashboard";
import { withRouter } from "react-router-dom";
import { useState, useEffect } from "react";
import Swal from "sweetalert2";
import { useHistory } from "react-router-dom";
function UpdateTeam(props) {
  const [data, setData] = useState([]);
  const [name, setName] = useState("");
  const [file, setFile] = useState("");
  const [employees, setEmployees] = useState("");
  const [description, setDescription] = useState("");

  let history = useHistory();
  useEffect(async () => {
    let result = await fetch(
      "http://localhost:8000/api/team/" + props.match.params.id
    );
    result = await result.json();
    setData(result);
    setName(result.name);
    setEmployees(result.employees);
    setDescription(result.description);
    setFile(result.file);
  }, []);

  async function editTeam(id) {
    const formData = new FormData();

    formData.append("file", file);
    formData.append("name", name);
    formData.append("employees", employees);
    formData.append("description", description);
    let result = await fetch(
      "http://localhost:8000/api/update_team/" + id + "?_method=PUT",
      {
        method: "POST",
        body: formData,
      }
    );
    Swal.fire("Good job!", "Team Updated Successfully", "success");
    history.push("/teams-list");
  }
  return (
    <div>
      <Dashboard />
      <div className="form-wrapper" style={{ margin: "5% 20% 0 20%" }}>
        <h1>Update a Team</h1>
        <input
          type="text"
          onChange={(e) => setName(e.target.value)}
          defaultValue={data.name}
          className="form-control"
        />
        <br />
        <input
          type="text"
          onChange={(e) => setEmployees(e.target.value)}
          defaultValue={data.employees}
          className="form-control"
        />{" "}
        <br />
        <input
          type="text"
          onChange={(e) => setDescription(e.target.value)}
          defaultValue={data.description}
          className="form-control"
        />{" "}
        <br />
        <input
          placeholder="Team's logo"
          type="file"
          onChange={(e) => setFile(e.target.files[0])}
          defaultValue={data.file_path}
          className="form-control"
        />{" "}
        <br />
        <img
          style={{ width: 300 }}
          src={"http://localhost:8000/" + data.file_path}
        />
        <br /> <br />
        <button
          onClick={() => editTeam(data.id)}
          className="btn btn-primary"
          style={{ marginBottom: "5%" }}
        >
          Update Team
        </button>
      </div>
    </div>
  );
}
export default withRouter(UpdateTeam);
