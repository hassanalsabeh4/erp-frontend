import React from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import EmployeesList from "./components/employees/employee-listing.component";
import CreateEmployee from "./components/employees/employeeAdd";
import UpdateEmployee from "./components/employees/edit-employee.component";
import ProjectList from "./components/projects/ProjectList";
import CreateProject from "./components/projects/ProjectAdd";
import UpdateProject from "./components/projects/ProjectUpdate";
import FileUploadComponent from "./components/FileUploadComponent";
import Login from "./components/login/Login"

import Welcome from "./components/welcom/welcome";
import Admins from "./components/admins/admins";
import ShowAdmins from "./components/admins/showadmins";
import UpdateAdmins from "./components/admins/update-admins";
import addTeam from "./components/team/teamAdd";
import TeamsList from "./components/team/teamList";
import CreateTeam from "./components/team/teamUpdate";
import Home from "./components/charts/home";
import Register from "./components/register/register";


axios.defaults.headers.post["Content-Type"] = "application/json";
axios.defaults.headers.post["Accept"] = "application/json";
axios.interceptors.request.use(function (config) {
  const token = localStorage.getItem("user-info");
  console.log(token);
  config.headers.Authorization = token ? `Bearer ${token}` : ``;
  return config;
});

function App() {
  return (
    <div>
      <Route path="/" exact component={Welcome} />
    
      <Route path="/home" exact component={Home} />
      <Route path="/login" exact component={Login} />

     
      <Route path="/register" exact component={Register} />
      <Route path="/admins" exact component={Admins} />
      <Route path="/show-admins" exact component={ShowAdmins} />
      <Route path="/update-admins/:id" exact component={UpdateAdmins} />
      <Route path="/employees-list" exact component={EmployeesList} />
      <Route path="/employees-add" exact component={CreateEmployee} />
      <Route path="/employees-update/:id" exact component={UpdateEmployee} />
      <Route path="/projects-list" exact component={ProjectList} />
      <Route path="/projects-add" exact component={CreateProject} />
      <Route path="/projects-update/:id" exact component={UpdateProject} />
      <Route path="/teams-add" exact component={addTeam} />
      <Route path="/teams-list" exact component={TeamsList} />
      <Route path="/teams-update/:id" exact component={CreateTeam} />
      <Route path="/upload" exact component={FileUploadComponent} />
    </div>
  );
}

export default App;
